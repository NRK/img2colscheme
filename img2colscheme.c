/*
 * This file is part of img2colscheme.
 *
 * img2colscheme is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * img2colscheme is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with img2colscheme. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>

#include "stb_image.h"

/* Typedefs */
typedef unsigned int     uint;
typedef unsigned short   ushort;
typedef unsigned long    ulong;
typedef unsigned char    uchar;
/* bools */
typedef unsigned char    Bool;
enum { False, True };

#if UINT_MAX == 4294967295UL
	typedef signed   int     i32;
	typedef unsigned int     u32;
	#define U32_X_FMT "X"
#elif ULONG_MAX == 4294967295UL
	typedef signed   long    i32;
	typedef unsigned long    u32;
	#define U32_X_FMT "lX"
#else
	#error "32 bit not available"
#endif

/* portable compiler attributes, only used for diagnostic purposes */
#ifndef __has_attribute
	#define __has_attribute(X) (0)
#endif
#if __has_attribute(format)
	#define ATTR_FMT(A, S, ARG) __attribute__ ((format (A, S, ARG)))
#else
	#define ATTR_FMT(A, S, ARG)
#endif
#if __has_attribute(noreturn)
	#define ATTR_NORETURN __attribute__ ((noreturn))
#else
	#define ATTR_NORETURN
#endif

typedef struct {
	u32 *col;
	size_t *cnt;
	size_t cap;
} ColorTable;

typedef struct {
	u32 col;
	u32 cnt;
} ColorCount;

typedef struct {
	ushort h   : 9;
	ushort s   : 7;
	ushort l   : 7;
	ushort idx : 9;
} HSL;

/* function prototype */
static void fatal(int errnum, const char *fmt, ...) ATTR_FMT(printf, 2, 3) ATTR_NORETURN;
static void * ecalloc(size_t nmemb, size_t size);
static void top_colors_insert(ColorCount c);
static ColorTable ct_create(size_t n);
static void ct_destroy(ColorTable ct);
static size_t ct_increment(ColorTable ct, u32 col);
static void ct_color_add(ColorTable ct, u32 col);
static HSL rgb_to_hsl(u32 col);
static u32 hsl_to_rgb(HSL c);
static void hsl_print(HSL col, const char *prefix, ...);
static int hue_cmp(const void *p1, const void *p2);
static void color_print_core(u32 col, const char *prefix, va_list ap);
static void color_print(u32 col, const char *prefix, ...) ATTR_FMT(printf, 2, 3);
static void gen_color_pallet(void);
#ifdef DEBUG
static void top_colors_dump(void);
#endif

/* macros */
#define DIFF(A, B)       ((A) > (B) ? (A) - (B) : (B) - (A))
#define ARRLEN(X)        (sizeof(X) / sizeof((X)[0]))
#define MAX(A, B)        ((A) > (B) ? (A) : (B))
#define MIN(A, B)        ((A) < (B) ? (A) : (B))
#define R(X)             (((X) & 0xFF0000) >> 16)
#define G(X)             (((X) & 0x00FF00) >>  8)
#define B(X)             (((X) & 0x0000FF) >>  0)

/* globals */
enum { TOP_COLORS_CAP = 256 };
static ColorCount top_colors[TOP_COLORS_CAP];

/*
 * function implementation
 */

#ifdef DEBUG
static void
top_colors_dump(void)
{
	uint i;
	for (i = 0; i < TOP_COLORS_CAP; ++i)
		color_print(top_colors[i].col, "%u", i);
}
#endif

static void
fatal(int errnum, const char *fmt, ...)
{
	va_list ap;

	fflush(stdout);
	va_start(ap, fmt);
	if (fmt)
		vfprintf(stderr, fmt, ap);
	va_end(ap);
	if (errnum)
		fprintf(stderr, "%s%s", fmt ? ": " : "", strerror(errnum));
	fputc('\n', stderr);

	exit(1);
}

static void *
ecalloc(size_t nmemb, size_t size)
{
	void *ret;

	if ((ret = calloc(nmemb, size)) == NULL)
		fatal(errno, "ecalloc");
	return ret;
}

static void
top_colors_insert(ColorCount c)
{
	uint i, k;

	if (top_colors[TOP_COLORS_CAP-1].cnt > c.cnt)
		return;

	/* TODO: either reduce top_colors size or use a linked list instead to avoid expensive memmove()s */
	for (i = 0; i < TOP_COLORS_CAP; ++i) {
		if (top_colors[i].cnt < c.cnt) {
			for (k = i; k < TOP_COLORS_CAP; ++k) {
				if (top_colors[k].col == c.col) {
					memmove(top_colors+k, top_colors+k+1,
					        (TOP_COLORS_CAP - k - 1) * sizeof(*top_colors));
					break;
				}
			}
			memmove(top_colors+i+1, top_colors+i, (TOP_COLORS_CAP - i - 1) * sizeof(*top_colors));
			top_colors[i] = c;
			return;
		}
	}
}

static ColorTable
ct_create(size_t n)
{
	ColorTable ct;
	ct.col = ecalloc(n, sizeof(*ct.col));
	ct.cnt = ecalloc(n, sizeof(*ct.cnt));
	ct.cap = n;
	return ct;
}

static void
ct_destroy(ColorTable ct)
{
	free(ct.col);
	free(ct.cnt);
}

static size_t
ct_increment(ColorTable ct, u32 col)
{
	size_t i;
	const size_t idx = col % ct.cap;
	Bool rolling = False;

	for (i = idx; i < ct.cap; ++i) {
		if (ct.col[i] == col) {
			return ++ct.cnt[i];
		} else if (ct.cnt[i] == 0) {
			ct.col[i] = col; /* cppcheck-suppress unreadVariable */
			return ++ct.cnt[i];
		}

		if (rolling && i >= idx)
			fatal(0, "%d: unreacheable", __LINE__);
		if (i == ct.cap) {
			i = 0;
			rolling = True;
		}
	}
	fatal(0, "%d: unreacheable", __LINE__);
	return 0; /* silence compiler warning */
}

static void
ct_color_add(ColorTable ct, u32 col)
{
	ColorCount c;
	size_t cnt;
	c.col = col;
	c.cnt = (u32)(cnt = ct_increment(ct, col));
	if (cnt > (u32)-1)
		fatal(0, "overflow");
	top_colors_insert(c);
}

static HSL
rgb_to_hsl(u32 col)
{
	HSL ret = {0};
	const int r = R(col);
	const int g = G(col);
	const int b = B(col);
	const int max = MAX(MAX(r, g), b);
	const int min = MIN(MIN(r, g), b);
	const int l = ((max + min) * 50) / 255;
	int s = 0;
	long h = 0; /* should work even if long == 32bits */

	if (max != min) {
		const int d = max - min;
		s = (d * 100) / max;
		if (max == r) {
			h = ((g - b) * 1000) / d + (g < b ? 6000 : 0);
		} else if (max == g) {
			h = ((b - r) * 1000) / d + 2000;
		} else {
			h = ((r - g) * 1000) / d + 4000;
		}
		h *= 6;
		h /= 100;
		if (h < 0)
			h += 360;
	}

	ret.h = (ushort)h;
	ret.l = (ushort)l;
	ret.s = (ushort)s;
	return ret;
}

static u32
hsl_to_rgb(HSL col)
{
	ulong f, p, q, t;
	ulong h, s, l;
	ulong r, g, b;

	if (col.s == 0) {
		const u32 tmp = (col.l * 255) / 100;
		return (tmp << 16) | (tmp << 8) | tmp;
	}

	h = (col.h * 1000) / 6;
	s = col.s;
	l = col.l;

	f = (h % 10000) / 100;
	p = l * (100 - s);
	q = l * (100 - (s * f) / 100);
	t = (l * (10000 - s * (100 - f))) / 100;

	l *= 100; /* scaled up x10000 */

	switch (h/10000) {
	case 0:
		r = l; g = t; b = p; break;
	case 1:
		r = q; g = l; b = p; break;
	case 2:
		r = p; g = l; b = t; break;
	case 3:
		r = p; g = q; b = l; break;
	case 4:
		r = t; g = p; b = l; break;
	default:
		r = l; g = p; b = q; break;
	}

	return (u32)(((r * 255) / 10000) << 16) |
	       (u32)(((g * 255) / 10000) <<  8) |
	       (u32)(((b * 255) / 10000) <<  0);
}

static void
color_print_core(u32 col, const char *prefix, va_list ap)
{
	const uint r = R(col);
	const uint g = G(col);
	const uint b = B(col);
	int padding = 8;
	int tmp = 0;

	fflush(stdout);
	if (prefix) {
		tmp = vfprintf(stdout, prefix, ap);
		tmp += printf(":");
	}

	padding -= tmp;
	while (padding --> 0)
		printf(" ");

	printf("#%06" U32_X_FMT "\t\t", col);
	printf("\033[38;2;%u;%u;%um\033[48;2;%u;%u;%umLMAO", r, g, b, r, g, b);
	printf("\033[m\n");
}

static void
color_print(u32 col, const char *prefix, ...)
{
	va_list ap;
	va_start(ap, prefix);
	color_print_core(col, prefix, ap);
	va_end(ap);
}

static void
hsl_print(HSL col, const char *prefix, ...)
{
	u32 rgb;
	va_list ap;

	rgb = col.idx < TOP_COLORS_CAP ? top_colors[col.idx].col : hsl_to_rgb(col);
	va_start(ap, prefix);
	color_print_core(rgb, prefix, ap);
	va_end(ap);
}

static int
hue_cmp(const void *p1, const void *p2)
{
	int h1 = ((const HSL *)p1)->h;
	int h2 = ((const HSL *)p2)->h;
	h1 = h1 > 340 ? h1 - 340 : h1 + 20;
	h2 = h2 > 340 ? h2 - 340 : h2 + 20;
	return h1 - h2;
}

static void
gen_color_pallet(void)
{
	uint i, n;
	HSL fg, bg;
	HSL seven[7];
	HSL prime[7];
	HSL unprime[7];
	size_t fg_cnt = 0, bg_cnt = 0;
	uint seven_head = 0, prime_head = 0, unprime_head = 0;
	uint avg_l = 0, avg_s = 0;

	memset(seven, UCHAR_MAX, sizeof(seven));
	memset(prime, UCHAR_MAX, sizeof(prime));
	memset(unprime, UCHAR_MAX, sizeof(unprime));
	fg.idx = bg.idx = TOP_COLORS_CAP;
	for (i = 0; i < TOP_COLORS_CAP; ++i) {
		HSL tmp = rgb_to_hsl(top_colors[i].col);
		if (tmp.l < 24 || (tmp.l < 32 && tmp.s < 24)) {
			if (bg_cnt < top_colors[i].cnt) {
				bg_cnt = top_colors[i].cnt;
				bg = tmp;
				bg.idx = (ushort)i;
			}
		} else if (tmp.l > 60 && tmp.s < 24) {
			if (fg_cnt < top_colors[i].cnt) {
				fg_cnt = top_colors[i].cnt;
				fg = tmp;
				fg.idx = (ushort)i;
			}
		} else if (tmp.l > 8) { /* cppcheck-suppress knownConditionTrueFalse */
			if (seven_head < ARRLEN(seven)) {
				uint k, d = UINT_MAX;
				for (k = 0; k < seven_head; ++k) {
					uint tmpd = DIFF(tmp.h, seven[k].h);
					if (d > tmpd)
						d = tmpd;
				}
				if (d >= 8) {
					seven[seven_head] = tmp;
					seven[seven_head].idx = (ushort)i;
					++seven_head;
				}
			}
		}
	}

	/* TODO: fix the color output. currently outputs meaningless numbers */
	if (fg.idx < TOP_COLORS_CAP) {
		color_print(top_colors[fg.idx].col, "fg");
	} else {
		printf("fg: not found\n");
	}
	if (bg.idx < TOP_COLORS_CAP) {
		color_print(top_colors[bg.idx].col, "bg");
	} else {
		printf("bg: not found\n");
	}

	if (seven_head < 3)
		fatal(0, "not enough colors!");

	for (i = 0; i < seven_head; ++i) {
		avg_l += seven[i].l;
		avg_s += seven[i].s;
	}
	avg_l /= seven_head;
	avg_s /= seven_head;
	for (i = 0; i < seven_head; ++i) {
		uint sd = DIFF(avg_s, seven[i].s);
		uint ld = DIFF(avg_l, seven[i].l);
		if (sd < 20 && ld < 16)
			prime[prime_head++] = seven[i];
		else
			unprime[unprime_head++] = seven[i];
	}

	for (i = 0, n = 0; prime_head < ARRLEN(prime); ++i, ++prime_head) {
		if (i < unprime_head) {
			prime[prime_head] = unprime[i];
			prime[prime_head].l = (ushort)avg_l;
			prime[prime_head].idx = TOP_COLORS_CAP;
		} else if (n+1 < prime_head) {
			if (n == 0) {
				qsort(prime, prime_head, sizeof(*prime), hue_cmp);
			}
			prime[prime_head].h = (prime[n].h + prime[n+1].h) / 2;
			prime[prime_head].s = (prime[n].s + prime[n+1].s) / 2;
			prime[prime_head].l = (ushort)avg_l;
			prime[prime_head].idx = TOP_COLORS_CAP;
			++n;
		}
	}

	if (prime_head < ARRLEN(prime))
		fatal(0, "couldn't generate enough colors");

	qsort(prime, prime_head, sizeof(*prime), hue_cmp);
	for (i = 0; i < ARRLEN(prime); ++i) {
		HSL t = prime[i];
		t.l += avg_l > 40 ? -6 : 6;
		unprime[i] = t;
		hsl_print(prime[i], "%u", i);
	}
	for (i = 0; i < ARRLEN(unprime); ++i)
		hsl_print(unprime[i], "%u", i + 7);
}

int
main(int argc, char *const argv[])
{
	int w, h, dummy;
	const char *file;
	uchar *img;
	size_t i;
	ColorTable ct;
	const uint channels = 4;
	const u32 even_mask = 252;
	const u32 even_out = (even_mask << 24) | (even_mask << 16) | (even_mask << 8) | even_mask;

	if (argc < 2)
		fatal(0, "no arguments");
	file = argv[1];

	img = stbi_load(file, &w, &h, &dummy, channels);
	if (img == NULL)
		fatal(0, "error loading image");
	ct = ct_create((ulong)w * (ulong)h); /* TODO: make the "hashtable" growable instead of allocating every pixel */
	/* TODO: "cartoonify"/"smoothen" the image before processing the colors */
	for (i = 0; i < (ulong)w * (ulong)h; ++i) {
		const size_t idx = i * channels;
		const size_t r = idx, g = idx+1, b = idx+2, a = idx+3; /* stbi_load() returns RGBA */
		const u32 c = ((u32)img[r] << 16) | ((u32)img[g] << 8) | (u32)img[b];
		if (img[a])
			ct_color_add(ct, c & even_out);
	}
	stbi_image_free(img);
	ct_destroy(ct);

	gen_color_pallet();

	return 0;
}
