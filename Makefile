# debug
_DFLAGS      ?= -g -fsanitize=address,undefined,leak
# optimizations
O_BASIC       = -pipe -march=native -Ofast
O_LTO         = -flto=auto -fuse-linker-plugin
O_GRAPHITE    = -fgraphite-identity -floop-nest-optimize
O_IPAPTA      = -fipa-pta
O_BUILTIN     = -fbuiltin
O_SEMINTERPOS = -fno-semantic-interposition
O_NOCOMMON    = -fno-common
O_NOPLT       = -fno-plt
O_NOPIE       = -no-pie
O_NOSSP       = -fno-stack-protector
OFLAGS = $(O_BASIC) $(O_LTO) $(O_GRAPHITE) $(O_IPAPTA) \
         $(O_SEMINTERPOS) $(O_NOCOMMON) $(O_NOPLT) \
         $(O_NOPIE) $(O_NOSSP) $(O_BUILTIN) \

# fallback if $(CC) != "gcc"
O_FALLBACK    = -O3

# warnings
STD    = c99
WGCC   = -Wlogical-op -Wcast-align=strict
WGCC  += -Wsuggest-attribute=malloc
WGCC  += -Wsuggest-attribute=pure -Wsuggest-attribute=const
WGCC  += -Wsuggest-attribute=noreturn -Wsuggest-attribute=cold
WCLANG = -Weverything
WCLANG += -Wno-disabled-macro-expansion -Wno-padded -Wno-format-nonliteral
WCLANG += -Wno-unreachable-code-return
WFLAGS = -std=$(STD) -Wall -Wextra -Wpedantic \
         -Wshadow -Wvla -Wpointer-arith -Wwrite-strings -Wfloat-equal \
         -Wcast-align -Wcast-qual -Wbad-function-cast \
         -Wstrict-overflow=2 -Wunreachable-code -Wformat=2 \
         -Wundef -Wstrict-prototypes -Wmissing-declarations \
         -Wmissing-prototypes -Wold-style-definition \
         $$(test "$(CC)" = "gcc" && printf "%s " $(WGCC)) \
         $$(test "$(CC)" = "clang" && printf "%s " $(WCLANG)) \

# CPPCHECK
CPPCHECK      = $$(command -v cppcheck 2>/dev/null || printf ":")
CPPCHECK_ARGS = --std=$(STD) --quiet --inline-suppr --force \
                --enable=performance,portability,style \
                --max-ctu-depth=8 -j8 \

CTIDY      = $$(command -v clang-tidy 2>/dev/null || printf ":")
CTIDY_ARGS = --quiet --warnings-as-errors="*" \
             --checks="$$(sed '/^\#/d' .clangtidychecks | paste -d ',' -s)"

# Cool stuff
CC       ?= cc
CFLAGS   ?= $$(test "$(CC)" = "gcc" && printf "%s " $(OFLAGS) || printf "%s " $(O_FALLBACK))
CFLAGS   += $(WFLAGS) $(DFLAGS)
CPPFLAGS  = $(DEBUG_CPP)
STRIP    ?= -s
LDFLAGS  ?= $(CFLAGS) $(STRIP)
LDLIBS    = -lm

BIN  = img2colscheme
OBJS = img2colscheme.o stb_image.o

.PHONY: clean
.SUFFIXES:
.SUFFIXES: .c .o

all: $(BIN)
$(OBJS): Makefile
stb_image.o: stb_image.h

$(BIN): $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) -o $@ $(LDLIBS)

c.o:
	$(CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ $<

debug:
	make BIN="$(BIN)-debug" DFLAGS="$(_DFLAGS)" DEBUG_CPP="-DDEBUG" STRIP="" all

analyze:
	make clean; make CC="gcc" OFLAGS="$(OFLAGS) -fanalyzer"
	make clean; make CC="clang" OFLAGS="-march=native -Ofast -flto"
	$(CPPCHECK) $(CPPCHECK_ARGS) img2colscheme.c
	$(CTIDY) $(CTIDY_ARGS) img2colscheme.c "--" -std=$(STD) $$(make CC=clang dump_cppflags)

dump_cppflags:
	@echo $(CPPFLAGS)

clean:
	rm -f *.o $(OBJS) $(BIN) $(BIN)-debug

