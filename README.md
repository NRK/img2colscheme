# img2colscheme

Generate a terminal 16 color-scheme out of an image.
Work in progress: see [TODO](#todo)

## Usage

```console
$ ./img2colscheme </path/to/image>
```

## Building

- Build Dependencies:
  * (GNU) make
  * C99 compiler

- Runtime Dependencies:
  * Standard C library

```console
$ make CC=gcc
```

## TODO

`img2colscheme` works best on "flat" images (e.g cartoon) with lots of distinct
colors. It's unable to cope properly on images which have a lot of similar
colors.

* Fix color-scheme generation on non-flat images.

grep the source to find more TODOs:

```console
$ grep -Hn -E 'TODO|FIXME' img2colscheme.c
```
